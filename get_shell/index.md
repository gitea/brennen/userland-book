0. get you a shell
==================

You don't have to have a shell at hand to get something out of this book.
Still, as with most practical subjects, you'll learn more if you try things out
as you go.  You shouldn't feel guilty about skipping this section.  It will
always be here later if you need it.

Not so long ago, it was common for schools and ISPs to hand out shell accounts
on big shared systems.  People learned the command line as a side effect of
reading their e-mail.

That doesn't happen as often now, but in the meanwhile computers have become
relatively cheap and free software is abundant.  If you're reading this on the
web, you can probably get access to a shell.  Some options follow.

get an account on a social unix server
--------------------------------------

Check out [tilde.town][tildetown]:

> tilde.town is an intentional digital community for making art, socializing, and
> learning. Unlike many online spaces, users interact with tilde.town through a
> direct connection instead of a web site. This means using a tool called ssh and
> other text based tools.

use a raspberry pi or beaglebone
--------------------------------

Do you have a single-board computer laying around?  Perfect.  If you already
run the standard Raspbian, Debian on a BeagleBone, or a similar-enough Linux,
you don't need much else.  I wrote most of this text on a Raspberry Pi, and the
example commands should all work there.

use a virtual machine
---------------------

A few options:

* [Use Vagrant to spin up a machine in Virtualbox](https://docs.vagrantup.com/v2/getting-started/index.html)
* [Use DigitalOcean to create a remotely-hosted VM running Linux](https://www.digitalocean.com/community/tutorials/how-to-create-your-first-digitalocean-droplet-virtual-server)
