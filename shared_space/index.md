7. the command line as as a shared world
========================================

In an earlier chapter, I wrote:

> You can think of the shell as a kind of environment you inhabit, in much
> the way your character inhabits an adventure game.

It turns out that sometimes there are other human inhabitants of this
environment.

Unix was built on a model known as "time-sharing".  This is an idea with a lot
of history, but the very short version is that when computers were rare and
expensive, it made sense for lots of people to be able to use them at once.
This is part of the story of how ideas like e-mail and chat were originally
born, well before networks took over the world:  As ways for the many users of
one computer to communicate on the same machine.

Says Dennis Ritchie:

> What we wanted to preserve was not just a good environment in which to do
> programming, but a system around which a fellowship could form. We knew from
> experience that the essence of communal computing, as supplied by
> remote-access, time-shared machines, is not just to type programs into a
> terminal instead of a keypunch, but to encourage close communication.

Times have changed, and while it's mundane to use software that's shared
between many users, it's not nearly as common as it once was for a bunch of us
to be logged into the same computer all at once.

-> ★ <-

In the mid 1990s, when I was first exposed to Unix, it was by opening up a
program called NCSA Telnet on one of the Macs at school and connecting to a
server called mother.esu1.k12.ne.us.

NCSA Telnet was a terminal, not unlike the kind that you use to open a shell on
your own Linux computer, a piece of software that itself emulated actual,
physical hardware from an earlier era.  Hardware terminals were basically very
simple computers with keyboards, screens, and just enough networking brains to
talk to a _real_ computer somewhere else.  You'll still come across these
scattered around big institutional environments.  The last time I looked over
the shoulder of an airline checkin desk clerk, for example, I saw green
monochrome text that was probably coming from an IBM mainframe somewhere
far away.

Part of what was exciting about being logged into a computer somewhere else
was that you could _talk to people_.

-> ★ <-

_{This chapter is a work in progress.}_
