chapters = $(shell cat chapters)

all: index.html

index.html: ${chapters} chapters render.pl footer.html header.html
	cat chapters | xargs ./render.pl | cat header.html - footer.html > $@

publish: index.html
	git push
	make
	scp index.html root@p1k3.com:/var/www/userland-book/

pubimages: publish
	scp images/* root@p1k3.com:/var/www/userland-book/images/
