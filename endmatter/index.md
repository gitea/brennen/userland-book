endmatter
=========

further reading
---------------

- _The Unix Programming Environment_ - Brian W. Kernighan, Rob Pike
- [The Evolution of the Unix Time-sharing System](http://cm.bell-labs.com/cm/cs/who/dmr/hist.html) - Dennis M. Ritchie
- [AT&T Archives: The UNIX Operating System](https://www.youtube.com/watch?v=tc4ROCJYbm0) (YouTube)
- [I had a couple drinks and woke up with 1,000 nerds](https://medium.com/message/tilde-club-i-had-a-couple-drinks-and-woke-up-with-1-000-nerds-a8904f0a2ebf) - Paul Ford

code
----

As of July 2018, source for this work can be found <a
href="https://code.p1k3.com/gitea/brennen/userland-book">on code.p1k3.com</a>.
I welcome feedback there, <a href="https://mastodon.social/brennen">on
Mastodon</a>, or by mail to userland@p1k3.com.

copying
-------

This work is licensed under a
<a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0/">Creative
Commons Attribution-ShareAlike 4.0 International License</a>.

<a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0/">
<img alt="Creative Commons License" src="images/by-sa-4.png" />
</a>
