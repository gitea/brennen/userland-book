And a lot of the time, modern Unix-like systems look like this:

-> <img src="../images/debian.png"> <-

...which isn't actually that unfamiliar, right?
